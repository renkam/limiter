# Throttling bandwidth with Go

Imagine a service that is serving big log files over raw TCP, you decided to implement simple QoS for an existing server.

The goal of the task is to create small Go package that would allow for throttling bandwidth for TCP connections.

Requirements:

- keep it simple
  - think of it as open-source software that builds upon existing solutions
- use only stdlib and/or supplementary repositories (golang.org/x/\*)
  - prefer reusing existing packages
  - e.g. https://pkg.go.dev/golang.org/x/time/rate?tab=doc
- the package should allow for:
  - setting bandwidth limit per server
  - setting bandwidth limit per connection
  - changing limits in runtime (applies to all existing connections)
- for a 30s transfer sample consumed bandwidth should be accurate +/- 5%


Reasonable deadline for submitting a solution is 8 days.
