package main

import (
	"log"
	"net"
	"os"
	"time"
)

const (
	address  = "127.0.2.1:6677"
	protocol = "tcp"

	bufLen   = 256
	interval = 3 * time.Second
)

func main() {
	l := log.New(os.Stdout, "client -> ", 0)
	conn, err := net.Dial("tcp", "127.0.2.1:6677")
	if err != nil {
		l.Fatalf("connecting to %v: %v", address, err)
	}

	buf := make([]byte, bufLen)
	sum := 0
	start := time.Now()
	nextPrint := time.Now().Add(interval)
	avg := sma(3)

	for i := 0; ; i++ {

		n, err := conn.Read(buf)
		if err != nil {
			l.Fatalf("reading error: %v", err)
		}

		sum += n

		if now := time.Now(); now.After(nextPrint) {
			duration := (time.Since(start).Seconds())
			nextPrint = now.Add(interval)
			speed := float64(sum) / duration / 1000
			start = now
			sum = 0

			l.Printf("speed: %10.2f KB (not KiB)\n", avg(speed))
		}
	}
}

// taken from https://rosettacode.org/wiki/Averages/Simple_moving_average#Go
// under license
// https://www.gnu.org/licenses/old-licenses/fdl-1.2.html
func sma(period int) func(float64) float64 {
	var i int
	var sum float64
	var storage = make([]float64, 0, period)

	return func(input float64) (avrg float64) {
		if len(storage) < period {
			sum += input
			storage = append(storage, input)
		}

		sum += input - storage[i]
		storage[i], i = input, (i+1)%period
		avrg = sum / float64(len(storage))

		return
	}
}
