package main

import (
	"flag"
	"log"
	"net"

	"bitbucket.org/renkam/limiter"

	"github.com/rjeczalik/netxtest"
)

func myLimitedListener(l net.Listener, limitGlobal, limitPerConn int) net.Listener {
	limited := limiter.NewListener(l, limitGlobal, limitPerConn)
	return limited
}

func main() {
	var test netxtest.LimitListenerTest

	test.RegisterFlags(flag.CommandLine)
	flag.Parse()

	if err := test.Run(myLimitedListener); err != nil {
		log.Fatal(err)
	}
}
