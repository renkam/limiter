package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"strings"

	"bitbucket.org/renkam/limiter"
)

const (
	address  = "127.0.2.1:6677"
	protocol = "tcp"

	repeatCount = 100000
)

func main() {
	logger := log.New(os.Stderr, address+" -> ", log.Ltime+log.Lmicroseconds)

	rate := flag.Int("limit", 0, "per connection Bytes/s rate limit (0 for no limit)")
	global := flag.Int("global", 6563600, "for all connectionr Bytes/s rate limit (0 for no limit)")
	flag.Parse()

	limiterPool := limiter.NewPool(*global, *rate)

	serve(logger, *global, *rate, &limiterPool)
}

func help() {
	fmt.Println("Type: `g <num>` for global limit;")
	fmt.Println("Type: `c <num>` for per connection limit;")
}

func serve(logger *log.Logger, global, rate int, limiterPool *limiter.Pool) {
	listener, err := net.Listen(protocol, address)
	if err != nil {
		logger.Fatalf("listening to %v error: %v", address, err)
	}

	defer listener.Close()

	logger.Printf("listening on %s with global limit %v B/s\n", address, global)

	for i := 0; ; i++ {
		conn, err := listener.Accept()
		if err != nil {
			logger.Fatalf("accepting connection error: %v", err)
		}

		data := strings.Repeat(fmt.Sprintf("%d ", i), repeatCount)
		limited := limiterPool.NewLimiter(conn)

		go handle(limited, data, logger)
	}
}

func handle(conn net.Conn, data string, logger *log.Logger) {
	logger.Printf("incomming connection from: %v\n", conn.RemoteAddr())

	defer conn.Close()
	if err := writeData(conn, data); err != nil {
		logger.Printf("error connection %v: %v\n", conn.RemoteAddr(), err)
	}

	logger.Printf("closed connection with: %v\n", conn.RemoteAddr())
}

func writeData(conn net.Conn, data string) error {
	sum := int64(0)

	for {
		count, err := conn.Write([]byte(data))
		sum += int64(count)

		if err != nil {
			return err
		}

	}
}
