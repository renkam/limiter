module bitbucket.org/renkam/limiter

go 1.14

require (
	github.com/rjeczalik/netxtest v0.0.0-20190924130813-765a8eee5d0b
	golang.org/x/time v0.0.0-20200630173020-3af7569d3a1e
)
