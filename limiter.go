package limiter

import (
	"context"
	"fmt"
	"net"

	"golang.org/x/time/rate"
)

const (
	burstDivider = 30
	noLimitBurst = 10 * 1024 * 1024
	maxBurst     = 4096
)

func NewLimiter(conn net.Conn, limit int) *Conn {
	l, b := calculateBurstLimit(limit)

	return &Conn{
		Conn:  conn,
		limit: rate.NewLimiter(l, b),
	}
}

func NewSharedLimiter(conn net.Conn, limit *rate.Limiter) *Conn {
	return &Conn{
		Conn:  conn,
		limit: limit,
	}
}

type Conn struct {
	net.Conn

	limit *rate.Limiter
}

func (l *Conn) Write(b []byte) (int, error) {
	count := 0

	for count < len(b) {
		chunk := len(b[count:])
		if chunk > l.limit.Burst() {
			chunk = l.limit.Burst()
		}

		if err := l.limit.WaitN(context.Background(), chunk); err != nil {
			return count, fmt.Errorf("rate limit waiting: %w", err)
		}

		n, err := l.Conn.Write(b[count : count+chunk])
		count += n

		if err != nil {
			return count, fmt.Errorf("rate limit writing to connection: %w", err)
		}

	}

	return count, nil
}

func (l *Conn) SetLimit(newLimit int) {
	li, b := calculateBurstLimit(newLimit)

	l.limit.SetLimit(li)
	l.limit.SetBurst(b)
}

func calculateBurstLimit(limit int) (rate.Limit, int) {
	l := rate.Limit(limit)

	if limit == 0 {
		return rate.Inf, noLimitBurst
	}

	b := limit / burstDivider
	if limit < burstDivider {
		b = 1
	}
	if b > maxBurst {
		b = maxBurst
	}

	return l, b
}
