package limiter

import "net"

func NewListener(l net.Listener, global, perConn int) *Listener {
	return &Listener{
		Listener: l,
		pool:     NewPool(global, perConn),
	}
}

type Listener struct {
	net.Listener

	pool Pool
}

func (l *Listener) Accept() (net.Conn, error) {
	c, err := l.Listener.Accept()
	if err != nil {
		return nil, err
	}

	return l.pool.NewLimiter(c), nil
}
