package limiter

import (
	"net"
	"sync"

	"golang.org/x/time/rate"
)

func NewPool(global, single int) Pool {
	l, b := calculateBurstLimit(global)

	return Pool{
		limit:    rate.NewLimiter(l, b),
		rate:     single,
		notifier: sync.NewCond(&sync.Mutex{}),
	}
}

type Pool struct {
	limit *rate.Limiter

	notifier *sync.Cond
	rate     int
}

func (p *Pool) NewLimiter(conn net.Conn) *Conn {
	return NewLimiter(NewSharedLimiter(conn, p.limit), p.rate)
}
